'''
Create an Army class and a Battle class.
This is similar to the Poker and Blackjack games made in CS 313E (shout-out to Dr. Mitra!):
    "Army" is analogous to "Card"
    "Battle" is analogous to "Game"
'''
# -------------------- ARMY --------------------
class Army(object):
    
    # constructor
    def __init__(self, name, location, action):
        # initialize these variables
        self.name = name
        self.location = location
        self.action = action # ['move/support/hold', 'city/army/Nothing']
        
        # update these variables to decide who wins
        self.supporting = []
        self.supported_by = []
        self.is_attacked = False
        self.dead = False
    
    # allow army to blindly move to new position, regardless of conflict
    # updates self.location
    def move(self):
        if self.action[0] == 'Move':
            self.location = self.action[1]
    
    # check if an army is receiving support from anyone
    # appends supporter to self.supported_by
    def check_support_received(self, other):
        if (self.name != other.name) and (other.action == ['Support', str(self.name)]):
            self.supported_by.append(other)
    
    # check if army is supporting anyone
    # appends army being supported to self.supporting
    def check_support_given(self, other):
        if (self.name != other.name) and (self.action == ['Support', str(other.name)]):
            self.supporting.append(other)
    
    # first pass at conflict post-move
    # updates self.is_attacked variable with T/F
    def check_attack_status(self, other):
        if (self.name != other.name) and (self.location == other.location):
            self.is_attacked = True
    
    # based on results of check_attack_status, update support lists
    def update_support_given(self):
        if self.is_attacked:
            self.supporting = []
    
    def update_support_received(self, other):
        if (other.is_attacked) and (other in self.supported_by):
            self.supported_by.remove(other)
    
    # second pass at conflict
    # update self.dead
    def check_conflict_again(self, other):
        if (self.name != other.name) and (self.location == other.location):
            if len(self.supported_by) > len(other.supported_by):
                other.dead = True
            elif len(self.supported_by) < len(other.supported_by):
                self.dead = True
            else:
                self.dead = True
                other.dead = True
    
    # reformat for output purposes
    def adjust(self):
        if self.dead:
            self.location = '[dead]'

        
# -------------------- BATTLE --------------------
class Battle(object):
    
    # constructor
    def __init__(self, players):
        self.players = players
    
    # carry out the battle
    def fight(self):
    
        # execute desired actions
        for i in self.players:
            i.move()
        
        # check support given and received
        for i in self.players:
            for j in self.players:
                i.check_support_given(j)
                i.check_support_received(j)
        
        # check attack status
        for i in self.players:
            for j in self.players:
                i.check_attack_status(j)
        
        # update support given
        for i in self.players:
            i.update_support_given()
        
        # update support received
        for i in self.players:
            for j in self.players:
                i.update_support_received(j)
        
        # re-check the conflicts
        for i in self.players:
            for j in self.players:
                i.check_conflict_again(j)
        
        # update self.location
        for i in self.players:
            i.adjust()
    
    # alphabetize list of players
    def alphabetize(self):
        self.players.sort(key = lambda x : x.name)


# ------------------------------------------------------------
'''
This part of the code is more like the Collatz program.
There are functions to read input, solve problem, and write output.
'''

# -------------------- READ INPUT --------------------

# takes in a string from RunDiplomacy.in
# returns instance of Army class
def diplomacy_read(s):
    a = s.split()
    
    # break up string into 3 attributes
    name = a[0]
    location = a[1]
    action = a[2:]
    
    # create a new Army instance
    new_army = Army(name, location, action)
    return new_army


# -------------------- SOLVE PROBLEM --------------------

# takes in a list of strings: name, location, action
# returns results of the battle
def diplomacy_eval(army_list):
    new_battle = Battle(army_list)
    new_battle.fight()
    new_battle.alphabetize()
    return new_battle.players


# -------------------- WRITE OUTPUT --------------------
# takes in results of the battle
# prints results in RunDiplomacy.tmp
def diplomacy_print(w, player):
    w.write(str(player.name) + ' ' + str(player.location) + '\n')


# solves the diplomacy problem
def diplomacy_solve(r, w):
    army_list = []
    for s in r:
        new_army = diplomacy_read(s)
        army_list.append(new_army)
    results = diplomacy_eval(army_list)
    for i in results:
        diplomacy_print(w, i)
