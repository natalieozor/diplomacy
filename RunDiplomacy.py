# imports
import sys
from Diplomacy import diplomacy_solve


# main method
if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)

